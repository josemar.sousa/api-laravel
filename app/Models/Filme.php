<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filme extends Model
{
    protected $fillable = [
        'titulo',
        'capa'
    ];

    public function rules()
    {
        return [
            'titulo' => 'required',
            'capa' => 'image'
        ];
    }

    public function arquivo($id)
    {
        $data = $this->find($id);
        return $data->image;
    }

    /*public function documento()
    {
        return $this->hasOne(Documento::class, 'cliente_id', 'id'); //hasOne 1 para 1
    }

    public function telefone()
    {
        return $this->hasMany(Telefone::class, 'cliente_id', 'id'); //hasMany Muitos para 1
    }*/
}
