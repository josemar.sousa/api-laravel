<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Http\Controllers\MasterApiController;

class ClienteApiController extends MasterApiController
{

    protected $model;
    protected $path = 'clientes';
    protected $upload = 'image';
    protected $width = 177;
    protected $height = 233;


    public function __construct(Cliente $clientes, Request $request)
    {
        $this->model = $clientes;
        $this->request = $request;
    }

    //Consulta utilizando id de outra tabela - inner join
    public function documento($id)
    {
        if (!$data = $this->model->with('documento')->find($id)) { //with('documento') chamando a função documento na class model cliente
            return response()->json(['error' => 'Nada foi encontrado'], 404);
        } else {
            return response()->json($data);
        }
     }

    //Consulta utilizando id de outra tabela - inner join
    public function telefone($id)
    {
        if (!$data = $this->model->with('telefone')->find($id)) { //with('telefone') chamando a função telefone na class model cliente
            return response()->json(['error' => 'Nada foi encontrado'], 404);
        } else {
            return response()->json($data);
        }
     }

    //Consulta utilizando id de outra tabela - inner join
    public function algudados($id)
    {
        if (!$data = $this->model->with('filmesAlugados')->find($id)) { //with('documento') chamando a função filmesAlugados na class model cliente
            return response()->json(['error' => 'Nada foi encontrado'], 404);
        } else {
            return response()->json($data);
        }
     }

}
