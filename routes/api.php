<?php
//Route::get('clientes', 'Api\ClienteApiController@index');

//ROTAS DE LOGIN
$this->post('login', 'Auth\AuthenticateController@authenticate');
$this->post('login-refresh', 'Auth\AuthenticateController@refreshToken');
$this->get('me', 'Auth\AuthenticateController@getAuthenticatedUser');

$this->group(['namespace' => 'Api'/*, 'middleware' => 'auth:api'*/], function () {
    //ROTAS DE CLIENTES
    $this->get('clientes/{id}/filmes-alugados', 'ClienteApiController@algudados');
    $this->get('clientes/{id}/documento', 'ClienteApiController@documento');
    $this->get('clientes/{id}/telefone', 'ClienteApiController@telefone');
    $this->apiResource('clientes', 'ClienteApiController');

    //ROTAS DE DOCUMENTOS DE CLIENTES
    $this->get('documento/{id}/cliente', 'DocumentoApiController@cliente');
    $this->apiResource('documento', 'DocumentoApiController');

    //ROTAS DE TELEFONES DE CLIENTES
    $this->get('telefone/{id}/cliente', 'TelefoneApiController@cliente');
    $this->apiResource('telefone', 'TelefoneApiController');

    //ROTAS DE TELEFONES DE CLIENTES
    $this->apiResource('filme', 'FilmesApiController');
});


/*
//ROTAS DE CLIENTES
    $this->get('clientes/{id}/filmes-alugados', 'Api\ClienteApiController@algudados');
    $this->get('clientes/{id}/documento', 'Api\ClienteApiController@documento');
    $this->get('clientes/{id}/telefone', 'Api\ClienteApiController@telefone');
    $this->apiResource('clientes', 'Api\ClienteApiController');

    //ROTAS DE DOCUMENTOS DE CLIENTES
    $this->get('documento/{id}/cliente', 'Api\DocumentoApiController@cliente');
    $this->apiResource('documento', 'Api\DocumentoApiController');

    //ROTAS DE TELEFONES DE CLIENTES
    $this->get('telefone/{id}/cliente', 'Api\TelefoneApiController@cliente');
    $this->apiResource('telefone', 'Api\TelefoneApiController');

    //ROTAS DE TELEFONES DE CLIENTES
    $this->apiResource('filme', 'Api\FilmesApiController');
*/
